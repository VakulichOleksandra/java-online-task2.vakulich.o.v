import java.util.Scanner;

public class PartyCalculator {

    public static void main(String[] args) {

        // get the party participants number
        System.out.print("Enter number of party participants: ");
        Scanner sc = new Scanner(System.in);
        int participantsNumber = sc.nextInt();
        sc.close();

        // calculate probability
        double probability = 100;

        for (int i = 0; i < participantsNumber-1; i++) {
            double participantsNumberWithoutMe = participantsNumber - 1;
            double subProbability = ((participantsNumberWithoutMe - i) / participantsNumberWithoutMe) * 100;
            probability = (subProbability/probability) * 100;
        }

        System.out.println("Probability that everyone at the party will hear the rumor before it stops propagating is:");
        System.out.println("" + probability + " % ");
    }
}
